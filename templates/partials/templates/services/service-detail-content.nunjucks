<h3>Understand that technology issues do not exclusively apply to "tech companies".</h3>

<p>Buchanan Ingersoll & Rooney's Health Care Information Technology Group creates additional value for our health care clients, who increasingly rely heavily on hardware, software and information technology to operate, compete and prosper in the health care industry. Whether we are ensuring that an appropriate IT transition agreement is in place in advance of a strategic merger, or advising on IT-related electronic medical record and HIPAA compliance issues, our Health Care IT Group has the relevant experience needed to effectively and efficiently represent our health care clients in technology transactions.</p>


<p>Our Health Care IT Group consists of lawyers who understand that technology issues do not exclusively apply to "tech companies," and that every business, regardless of industry, is impacted by technology. Because our clients include both vendors and customers, we have insight into the factors driving technology transactions; we understand the "business" of technology. The depth and diversity of our industry and health care experiences allow us to bring a cost-effective approach to our practice, applying legal knowledge, practical business experience and sound technical knowledge when advising clients on the acquisition, use, development and commercial exploitation of technology.</p>


<p>In addition to the information technology-related matters we have handled for our health care clients, Buchanan's Technology Transactions Group has substantial experience in handling these types of issues for clients in any industry. </p>

<h4>The following are just a few examples of the broad range of transactions with which we get involved on behalf of our health care clients:</h4>

<ul>
	<li>Enterprise-wide licenses for multi-hospital systems</li>
	<li>IT outsourcing</li>
	<li>HIPAA compliance</li>
	<li>Acquisition of complex IT systems (such as picture archiving and communication systems (PACS)</li>
	<li>Proprietary and legacy solutions, maintenance, support and service level (SLA) agreements</li>
	<li>Website development agreements</li>
	<li>Open source software issues</li>
	<li>Application service provider (ASP) and Software-as-a-Service (SaaS) agreements</li>
	<li>Internet terms of use and privacy policy issues</li>
	<li>Internet domain name acquisition, transfer and dispute resolution issues</li>
	<li>Electronic contracting arrangements</li>
	<li>Private sector/university collaboration agreements</li>
	<li>Biotechnology transfer agreements</li>
	<li>Content licensing arrangements</li>
	<li>IT usage policies</li>
</ul>