<h3>Our commitment to diversity starts at the top.</h3>

<h4>Our Mission</h4>
<p>The different backgrounds, experiences and perspectives of our attorneys and government relations professionals are a critical source of the firm’s vitality, enrich the legal services that we provide our clients and enhance the future success of the firm. We are committed to supporting and sustaining a culture of inclusion that reflects both the ever-increasing diversity of our clients and the interconnectedness of the world in the 21st century. We recognize that only by fostering such a workplace culture will the firm continue to attract the best talent and support the development of long-term client relationships.</p>

<div class="customVideoOverlay">
	<button id="playbutton" style="background-image: url('{{ site_url }}assets/images/careers/video-overlay.jpg');">
		<div class="text">
			<div class="top">FIND OUT WHAT IT MEANS TO</div>
			<div class="bottom">KNOW GREATER PARTNERSHIP</div>
		</div>
	</button>
	<iframe src="https://player.vimeo.com/video/70401221" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

<h4>Leadership</h4>

<p>The purpose and direction of the firm’s diversity initiatives are guided by the leadership of the Chief Diversity & Inclusion Officer, the Diversity & Inclusion Committee and the Women’s Business Development Committee. Our commitment to diversity starts at the top, and our efforts are guided by our CEO’s vision and dedication. The Chief Diversity & Inclusion Officer and the Director of Professional Development & Diversity travel regularly to meet with individuals in each office to discuss their integration, work satisfaction and goals.</p>

<p>Outside of the firm, Buchanan has proudly partnered with various professional, educational and community groups that are dedicated to diversity, such as the University of Pittsburgh's Center for Race and Social Problems, the Philadelphia Diversity Law Group, the Allegheny County Bar Association and the National Association for Women Lawyers. Buchanan is a member in good standing of the Law Firm Affiliate Network of the Minority Corporate Counsel Association and has signed onto the Association's core diversity principles.</p>

<h4>Diversity Initiatives</h4>
<p>The firm has undertaken many initiatives over the years to address unique challenges facing women and diverse attorneys as they develop their practices. Our initiatives have included individual and group coaching, leadership and advancement training and presentations, and networking events, among many others.</p>

<p>The firm has held numerous coaching programs for women and diverse attorneys, including intensive one-year business development programs for groups of experienced women in the firm. The firm also has held mentoring and training opportunities, such as a formal sponsorship program for diverse attorneys on the cusp of promotion to shareholder and a “Tips from the Top” program linking senior associates looking forward to promotion to counsel with firm leaders in diverse mentoring circles. Topics in these programs have included confidence-building, taking control of one’s own career and planning for success, building relationships, networking and assessing the needs of clients. The firm seeks to involve all attorneys and government relations professionals in its diversity efforts in numerous ways, including encouraging all attorneys to use their professional and personal networks to recruit diverse lawyers to the firm and conducting firm-wide training on unconscious bias and cultural sensitivities.</p>