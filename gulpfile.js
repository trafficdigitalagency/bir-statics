var gulp            = require('gulp'),
    jshint          = require('gulp-jshint'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    concat          = require('gulp-concat'),
    prefix          = require('gulp-autoprefixer'),
    gulpif          = require('gulp-if'),
    browserSync     = require('browser-sync').create(),
    nunjucksRender  = require('gulp-nunjucks-render'),
    gls             = require('gulp-live-server');
    // htmlInjector    = require("bs-html-injector");

    //Variables
    input  = {
      'sassMaster'  : 'assets/sass/styles.sass',
      'sassWatch'   : 'assets/sass/**/*.sass'
    }

    output = {
      'css' : 'dist/css/'
    }
//Gulp Tasks
gulp.task('build', ['jshint','build-js-vendor', 'build-js','build-css','watch', 'nunjucks']);
gulp.task('default', ['watch', 'browserSync', 'build-js']);

//liveServer
gulp.task('serve', function(){
  var server = gls.static('/', 8888);
  server.start();
});

//Build CSS
gulp.task('build-css', function() {
  return gulp.src(input.sassMaster)
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}))
      // .pipe(sass())
      // .pipe(prefix("last 3 versions", "> 1%", "ie 8", "ie 7", "ie 6"))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.css))
    .pipe(browserSync.reload({
      stream: true
    }));
});

//Build CSS
gulp.task('build-pdf-css', function() {
  return gulp.src('briefcasePdf/pdf.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(prefix("last 3 versions", "> 1%", "ie 8", "ie 7", "ie 6"))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.css))
    .pipe(browserSync.reload({
      stream: true
    }));
});

//build nunjucks
gulp.task('nunjucks', function(){
  //nunjucks stuff
  // nunjucksRender.nunjucks.configure(['templates'], {watch: false});

  //Gets .html and .nunjucks files in pages
  return gulp.src('pages/**/*.nunjucks')
  //Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['templates'] // String or Array
    }))
  // output files in folder
  .pipe(gulp.dest('./'))
  .pipe(browserSync.reload({
    stream: true
  }));
});

//Build JS
gulp.task('build-js', function(){
  gulp.src([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/jquery-ui/jquery-ui.min.js',
    'bower_components/modernizr.min.js',
    'bower_components/gsap/src/minified/TimelineMax.min.js',
    'bower_components/gsap/src/minified/TweenMax.min.js',
    'bower_components/gsap/src/minified/plugins/CSSPlugin.min.js',
    'bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js',
    'bower_components/select2/dist/js/select2.full.min.js',
    'bower_components/slick-carousel/slick/slick.js',
    'bower_components/jquery.fitvids/jquery.fitvids.js',
    'bower_components/jquery-validation/dist/jquery.validate.min.js',
    'bower_components/kbw-plugin/dist/js/jquery.plugin.min.js',
    'bower_components/kbw-datepick/dist/js/jquery.datepick.min.js',
    'assets/js/jQuery/jquery.ui.touch-punch.min.js',
    'assets/js/custom.js'
  ])
  .pipe(concat('main.js'))
  .pipe(gulp.dest('dist/js'))
});

// JShint
gulp.task('jshint', function() {
  return gulp.src('assets/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});


//browserSync
gulp.task('browserSync', function() {
    // browserSync.use(htmlInjector, {
    //   files: "**/*.html"
    // });
    browserSync.init({
        proxy: "http://bir:8888/",
        files: ['**/*.html','**/*.js'],
        watchTask: true,
    });
});


//Watch Tasks
gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js', ['jshint', 'build-js']);
  // gulp.watch("**/*.html", htmlInjector);
  gulp.watch(input.sassWatch, ['build-css']);
  gulp.watch('briefcasePdf/*.sass', ['build-pdf-css']);
  // watch nunjucks stuff
  gulp.watch([
      'templates/**/*',
      'pages/*.nunjucks',
    ], ['nunjucks']
  )
});
