/** function that returns true based on current screen size
	0 = 0 to 1129px
	1 = 1130px+

**/
function screenSize(screensize) {
	if ($('#screen-size').css('z-index') >= screensize) {
		return true;
	} else {
		return false;
	}
}


// Add all initializing JS inside document.ready

$(document).ready(function(){

	// mobile menu
	$('.nav-mobile .hamburger-btn, .sliding-panel-fade-screen').on('click',function (e) {
		e.preventDefault();
		$('#main-nav, .sliding-panel-fade-screen').toggleClass('is-visible');
		$('html').toggleClass('no-scroll');
	});

	// mobile drop sub menu
	$('#main-nav nav > ul li.has-sub_menu > a').on('click',function (e) {
		e.preventDefault();
		if($(this).parent().hasClass('active') || $(this).parent().hasClass('open')) {
			$('#main-nav .has-sub_menu.open').removeClass('open');
			$(this).next('.sub-menu').slideUp(400, function() {
				$('#main-nav .has-sub_menu.active').removeClass('active');
			});
		} else {
			$('#main-nav .has-sub_menu.active .sub-menu').slideUp();
			$('#main-nav .has-sub_menu.open .sub-menu').slideUp();
			$('#main-nav .has-sub_menu.active').removeClass('active');
			$('#main-nav .has-sub_menu.open').removeClass('open');
			$(this).parent().addClass('open');
			$(this).next('.sub-menu').slideDown();
		}
	});

	// Desktop Search Show/Hide on scroll Feature

	function desktopScrollShowHide() {
		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $('.search-desktop').outerHeight();
		$(window).scroll(function(event){
			didScroll = true;
		});

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(this).scrollTop();
			// Make sure they scroll more than delta
			if(Math.abs(lastScrollTop - st) <= delta)
				return;

			// If they scrolled down and are past the navbar, add class .search-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight){
				// Scroll Down
				$('.search-desktop').removeClass('search-down').addClass('search-up');
				$('.secondary-nav').css('top', '0');
			} else {
				// Scroll Up
				if(st + $(window).height() < $(document).height()) {
					$('.search-desktop').removeClass('search-up').addClass('search-down');
					var searchBarHeight = $('.search-desktop').innerHeight() - 2;
					console.log('search down');
					if($('.secondary-nav').hasClass('secondary-nav--sticky')) {
						$('.secondary-nav').css('top', searchBarHeight +'px');
					} else {
						$('.secondary-nav').css('top', '0');
					}
				}
			}

			lastScrollTop = st;
		}
	}
	desktopScrollShowHide();

	// Secondary nav sticky function
	function stickySecondary() {
		var stickyElement = $('.secondary-nav');
		var stickyHeight = stickyElement.outerHeight();
		var top = stickyElement.offset().top - parseFloat(stickyElement.css('margin-top'));

		$(window).scroll(function (event) {
			var y = $(this).scrollTop();
			if (y >= top && screenSize(1)) {
				stickyElement.addClass('secondary-nav--sticky');
				$('.secondary-nav .module-filter').addClass('module-filter--collapsed');
				$('.page-content').css('padding-top', stickyHeight+'px');
			} else {
				stickyElement.removeClass('secondary-nav--sticky');
				$('.secondary-nav .module-filter').removeClass('module-filter--collapsed');
				$('.page-content').css('padding-top', '0');
			}
		});
	}

	// If the secondary nav element exists then trigger secondary nav sticky function
	if($('.secondary-nav').length) {
		stickySecondary();
	}


	$('.keyword-dropdown-filter__collapsed-btn').on('click touchstart',function (e) {
		e.preventDefault();
		$('.secondary-nav .module-filter').removeClass('module-filter--collapsed');
	});


	// Search Results Drop Down
	$('.search-input').focus( function() {
		$('.search-fade-screen').addClass('search-fade-screen--active');
		searchActive(true);
	});

	$('.search-input').blur( function() {
		if($(this).val() === ''){
			searchActive(false);
			$('.search-fade-screen').removeClass('search-fade-screen--active');
			searchResultsActive(false);
			$('.search-toggle').removeClass('active');
			$('.search-input').val('');
		}
	});

	$('.search-input').keypress( function() {
		$('.search-toggle').addClass('active');
		searchResultsActive(true);
	});

	// update mobile search in real time when desktop input added
	$('.search-desktop .search-input').change( function() {
		$('.nav-mobile .search-input').val( this.value );
	});

	// update desktop search in real time when mobile input added
	$('.nav-mobile .search-input').change( function() {
		$('.search-desktop .search-input').val( this.value );
	});


	$('.search-toggle').on('click touchstart',function (e) {
		closeSearch($(this));
	});

	function searchActive(state) {
		if(state === true) {
			$('.search-desktop').addClass('search-desktop--active');
			$('.search-mobile').addClass('search-mobile--active');
		}
		else if(state === false) {
			$('.search-desktop').removeClass('search-desktop--active');
			$('.search-mobile').removeClass('search-mobile--active');
		}
	}

	function searchResultsActive(state) {
		if(state === true) {
			$('.search-mobile .search-results, .search-desktop .search-results').slideDown();
			$('.search-mobile .search-results, .search-desktop .search-results').addClass('search-results--open');
		}
		else if (state === false) {
			$('.search-mobile .search-results, .search-desktop .search-results').removeClass('search-results--open');
			$('.search-mobile .search-results, .search-desktop .search-results').slideUp();
		}
	}
	function closeSearch(button) {
		if($(button).hasClass('active')) {
			searchResultsActive(false);
			setTimeout(function(){
				searchActive(false);
			}, 500);
			$('.search-toggle').removeClass('active');
			$('.search-input').val('');
			$('.search-fade-screen').removeClass('search-fade-screen--active');
		}
	}

	// datepick.js for datepicker filter
	$('.datepick-js').datepick({
		rangeSelect: true,
		monthsToShow: 2,
		pickerClass: 'date-filter',
		showAnim: 'fadeIn',
		prevText: '',
		nextText: '',
		onShow: function(picker) {
			$('.datepick-month.first .datepick-month-header .datepick-month-year:nth-child(2)').clone(true).prependTo('.datepick-nav');
			var trimmedMonth = picker.find('.datepick-month.last .datepick-month-header').text().split(' ')[0];
			picker.find('.datepick-month.last .datepick-month-header').text(trimmedMonth);

			// Append Clear Button
			$('.datepick-ctrl').append('<button class="clear-all">Clear All</button>');
			// Clear date but don't close modal
			$('button.clear-all').on('click touchstart',function (e) {
				$('.datepick-js').datepick('setDate', []);
			});
		}
	});
	// $('.datepick-js').datepick('show');



	// select2 all the boxes
	var selectBoxes = [
		["location", 'Location'],
		["professional", 'Professional'],
		["type", 'Type'],
		["date-range", 'Date range'],
		["service-industry", 'Service & Industry'],
		["schools", 'Schools']
	];

	// desktop select2 boxes for filter
	for (var i = 0; i < selectBoxes.length; i++) {
		$('section.module-filter select#' + selectBoxes[i][0]).select2({
			dropdownParent: $('.module-filter .keyword-dropdown-filter'),
			placeholder: selectBoxes[i][1],
			// closeOnSelect: false,
		});
	}

	// mobile select2 boxes for filter
	for (var z = 0; z < selectBoxes.length; z++) {
		$('.modal-mobile_filter .wrap-select').each(function(index) {
			$(this).find('select#' + selectBoxes[z][0]).select2({
				dropdownParent: $(this),
				placeholder: selectBoxes[z][1],
				minimumResultsForSearch: Infinity,
				// closeOnSelect: false
			});
		});
	}

	// open mobile filter
	$('button.mobile-filter').on('click touchstart',function (e) {
		e.preventDefault();
		$('.modal-mobile_filter').slideDown();
	});

	//close mobile filter
	$('.modal-mobile_filter button.close').on('click touchstart',function (e) {
		e.preventDefault();
		$('.modal-mobile_filter').slideUp();
	});

	// clear mobile filters
	$('.modal-mobile_filter button.clear-all').on('click touchstart',function (e) {
		e.preventDefault();
		console.log('hello');
		$('.modal-mobile_filter select').val(null).trigger("change");
	});


	// view all services list template
	// $('section.module-all_service_list > div > ul > li.has-sub_menu > a').on('click touchstart',function (e) {
	// 	e.preventDefault();
	// 	$(this).next('.sub-menu').slideToggle();
	// 	$(this).parent().toggleClass('open');
	// });

	function splitServiceList() {
		var listLength = $('section.module-all_service_list > .row > ul > li').length,
			middlePoint = (listLength / 2) ;
		if( middlePoint % 1 !== 0) { // odd items
			middlePoint = Math.round(middlePoint);
		}
		$("section.module-all_service_list > .row > ul > li").slice(0, 0 + middlePoint).wrapAll("<div class='col-left'>");
		$("section.module-all_service_list > .row > ul > li").slice(0, listLength).wrapAll("<div class='col-right'>");
	}
	// splitServiceList();

	// vcard_slider module
	$('section.module-vcard_slider .slider').slick({
		slide: '.vcard-set',
		slidesToShow: 1,
		dots: true,
	});

	// content_card_slider module
	$('section.module-content_card_slider .slider').slick({
		slide: '.card-set',
		slidesToShow: 1,
		dots: true,
	});

	// callout_slider module
	$('section.module-callout_slider .slider').slick({
		slide: '.slide',
		slidesToShow: 1,
		dots: true,
		arrows: false
	});

	// offset_slider
	$('section.module-offset_slider .slider-mobile').slick({
		slide: '.col-card',
		slidesToShow: 1,
		centerMode: true,
		infinite: false,
		centerPadding: '45px',
		dots: true,
		arrows: false
	});

	$('section.module-offset_slider .slider-desktop').slick({
		slide: '.card-set',
		slidesToShow: 1,
		centerMode: true,
		infinite: false,
		centerPadding: '85px',
		dots: true,
		arrows: false
	});


	/**
	 * function to load more 'items' in a 'container'
	 * @param  {string} container   the content container selector
	 * @param  {string} item        the item selector you want to load
	 * @param  {int} initialItems 	the number of items to load initially
	 * @param  {int} itemsToLoad  	the number of items to load on click
	 * @return {null}
	 */
	function loadMore(container, item, initialItems, itemsToLoad) {
		size = $(container + ' ' + item).size();
		$(container + ' ' + item + ':lt('+initialItems+')').show();
		$(container + ' .btn-load_more a').on('click touchstart',function (e) {
			e.preventDefault();
			initialItems = ( initialItems + itemsToLoad <= size) ? initialItems + itemsToLoad : size;
			$(container + ' ' + item + ':lt('+ initialItems +')').slideDown();
			if(initialItems == size){
				$(this).hide();
			}
		});
	}
	// template-insights
	if ( $( ".module-content_cards_recent" ).length ) {
		loadMore('.module-content_cards_recent', '.col-card', 12, 4);
	}
	// template articles & advisories
	if ( $( ".module-articles_list" ).length ) {
		loadMore('.module-articles_list', '.article', 5, 4);
	}

	// people grid
	if ( $( "#template-people .module-people_grid" ).length ) {
		loadMore('#template-people .module-people_grid', '.vcard', 8, 4);
	}

	// responsive videos
	$(".module-post_content").fitVids();

	// form validation
	$("form").validate({
		onfocusout: function(element) {
			this.element(element);
		}
	});

	//PDF Modal
	function pdfModal() {
		// Open Modal
		$('.pdf-modal-trigger').on('click touchstart',function (e) {
			e.preventDefault();
			$('.pdf-modal').addClass('pdf-modal--active');
			$("body").addClass("modal-open");
		});
		// Close Modal
		$(".modal-fade-screen, .modal-close").on("click", function() {
			$('.pdf-modal').removeClass('pdf-modal--active');
			$("body").removeClass("modal-open");
		});
		$(".modal-inner").on("click", function(e) {
			e.stopPropagation();
		});

		// This part requires that the briefcase javascript has been loaded in and doing what it is supposed to do
		$("[type=submit]").click(function (event) {
			event.preventDefault();
			briefcase = JSON.parse(localStorage.briefcase);

			var params = {
				token:  briefcase.token,
				emailFrom: $("input[name=fromaddress]").val(),
				emailTo: $("input[name=toaddress]").val(),
				documentName: $("input[name=documentname]").val(),
				message: $("textarea[name=message]").val()
			};

			$.getJSON("http://www.bipc.com/briefcasebir/web/briefcase/email", params).done(function (data) {
				$('.pdf-modal__status-message--fail').hide();
				$('.pdf-modal__status-message--success').show('slow' ,function(){
					$(this).animateCss('pulse');
				})
			}).fail(function () {
				$('.pdf-modal__status-message--fail').show('slow' ,function(){
					$(this).animateCss('shake');
				})
			});

			$.post("http://www.bipc.com/my-briefcase", {
				toaddress: params.emailFrom,
				fromaddress: params.emailTo,
				documentname: params.documentName,
				message: params.message
			});

		});
	}
	pdfModal();

	$("a.briefcaseAdd").click(function () {
		var bcNum = $("a.briefcase span.notification").html();
		if ($(this).attr("added")) {
			return;
		}
		$(this).addClass('animated pulse');
		$("a.briefcase span.notification").html(parseInt(bcNum, 10) + 1);
		$(this).attr({ added: "briefcase" });
	});

	$("a.briefcase span.notification").html(function () {
		if (typeof(localStorage.briefcase) === 'undefined') {
			return 0;
		}
		if (typeof(JSON.parse(localStorage.briefcase).items) === 'undefined') {
			return 0;
		}

		var briefcase = JSON.parse(localStorage.briefcase);
		return briefcase.items.length;
	});

	// print bio
	$('.contact-widgets a.print').on('click touchstart',function (e) {
		e.preventDefault();
		window.print();
	});

	if (sessionStorage.getItem('protip') !== 'false') {
		$( ".hero-briefcase__add-btn" ).after( "<div class='hero-briefcase__add-btn-pro-tip'>Pro-Tip <hr>Easily compile pages that you'd like to save or share. Add a page to your briefcase by clicking this button and then visit your briefcase to download.</div>" );
	}

	$('.hero-briefcase__add-btn-pro-tip').on('click', function(e) {
		e.preventDefault();
		$(this).fadeOut(500);
		sessionStorage.setItem('protip', 'false');
	});

	// vimeo custom play image
	function videoPlayImage() {
	  document.getElementById("playbutton").disabled = true;
	  var player = $('iframe');
	  var playerOrigin = '*';
	  // Listen for messages from the player
	  if (window.addEventListener) {
		window.addEventListener('message', onMessageReceived, false);
	  } else {
		window.attachEvent('onmessage', onMessageReceived, false);
	  }

	  function onMessageReceived(event) {
		var data = JSON.parse(event.data);
		console.log(data.event);
		if (data.event === "ready") {
		  //attach ready function to the image
		  document.getElementById("playbutton").disabled = false;
		  $('#playbutton').click(function() {
			player[0].contentWindow.postMessage({
			  "method": "play"
			}, playerOrigin);
			$(this).fadeOut(750);
		  });

		}
	  }
	}
	if($('#').length) {
		videoPlayImage();
	}
	// https://github.com/daneden/animate.css
	// for use to add css animations
	$.fn.extend({
		animateCss: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});
});
